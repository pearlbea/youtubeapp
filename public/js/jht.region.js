/**
 * sets and gets region
 * @type {jht|*|{}}
 */

/*jslint
 browser : true, devel : true, plusplus : true, white: true,
 nomen: true, todo: true
 */
/*global jht */

jht.region = (function() {

    'use strict';

    var region, setRegion, getRegion, getRegions;

    setRegion = function(id) {
        jht.main.stateMap.region = id;
    };

    getRegion = function() {
        return region;
    };

    getRegions = function(){
        gapi.client.youtube.i18nRegions.list({
            part: 'snippet'
        }).
        execute(function(response){
            jht.main.showRegions(response);
        });
    };

    return {
        getRegions : getRegions,
        setRegion : setRegion,
        getRegion : getRegion
    };



}());
