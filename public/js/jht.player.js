/*jslint
 browser : true, devel : true, plusplus : true, white: true,
 nomen: true, todo: true
 */
/*global jht, $, jQuery */
jht.player = (function($){

    'use strict';

    var $player, init, loadVideo, pauseVideo, closePlayer;

    loadVideo = function( id ){

        $player.loadVideoById({
            'videoId' : id,
            'suggestedQuality' :'default'
        });
    };

    pauseVideo = function(){
        $player.pauseVideo();
    };

    closePlayer = function(){
        pauseVideo();
        $('#playerSkin').css('width', '0');
        jht.shell.loadActiveChannel();
    };

    init = function($div){

        $player = $div;

        $('#playerSkin').on('click', '#close', function(){
          closePlayer();
        });

    };

    return{
        init: init,
        loadVideo:loadVideo,
        closePlayer : closePlayer

    };

}(jQuery));

