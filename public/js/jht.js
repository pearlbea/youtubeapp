/*jslint
 browser : true, devel : true, plusplus : true, white: true,
 nomen: true, todo: true
 */
/*global jht, $, jQuery */

var jht = (function(){

    'use strict';

    var initModule;

    initModule = function( $div ){
        jht.shell.initModule($div);
        jht.google.initModule();
    };

    return {
        initModule : initModule
    };

}());
