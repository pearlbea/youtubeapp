/*jslint
 browser : true, devel : true, plusplus : true, white: true,
 nomen: true, todo: true
 */
/*global jht, $, jQuery, _ */

jht.shell = (function($) {

    'use strict';

    var initModule, displayGrid, displayLabel,
        lookUpLabel, addEventListeners, loadVideo,
        formatCaptions, formatDate, formatDuration,
        activateChannel,loadActiveChannel,
        jqueryMap = {
            $label : $('#label'),
            $menu : $('#menu'),
            $player : $('#ytplayer')
        };

    lookUpLabel = function(id){
        var label = {
            0 : 'Popular',
            10: 'Music',
            17: 'Sports',
            20: 'Gaming',
            27: 'Education',
            30: 'Movies',
            25: 'News'
        };
        return label[id];
    };

    displayLabel = function(id){
        var text = lookUpLabel(id);
        jqueryMap.$label.text(text);
    };

    formatDate = function(timestamp){
        return new Date(timestamp).toDateString();
    };

    formatDuration = function(duration){
        return duration.slice(2).toLowerCase();
    };

    formatCaptions = function(data){
        _.each(data.items, function(item){
            item.contentDetails.runtime = formatDuration(item.contentDetails.duration);
            item.snippet.pretty_date = formatDate(item.snippet.publishedAt);
        });
        return data;
    };

    displayGrid = function(data, id){
        var template = _.template( $('#vidGrid').html()),
            pretty_data = formatCaptions(data);
        jqueryMap.$container.empty().append( template( pretty_data ) );
        displayLabel(id);
    };

    loadVideo = function(id){
        jqueryMap.$container.empty();
        $("#playerSkin").css('width', '1097px');
        jht.player.loadVideo(id);
    };

    loadActiveChannel = function(){
        $('.channel.active').trigger('click');
    };

    activateChannel = function($this){
        $('.channel').removeClass('active');
        $this.addClass('active');

    };

    addEventListeners = function(){

        jqueryMap.$menu.on('click', '.channel', function(){
            var $this = $(this),
                id = $this.attr('data-id');
            activateChannel($this);
            jht.google.getVideos(id);
        });

        jqueryMap.$container.on('click', '.vid', function(){
            var id = $(this).attr('data-id');
            loadVideo(id);
        });

    };

    initModule = function($div){
        jqueryMap.$container = $div;
        addEventListeners();
    };

    return {
        initModule : initModule,
        jqueryMap : jqueryMap,
        displayGrid : displayGrid,
        loadVideo : loadVideo,
        loadActiveChannel : loadActiveChannel
    };


}(jQuery));
