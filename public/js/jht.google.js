/*jslint
 browser : true, devel : true, plusplus : true, white: true,
 nomen: true, todo: true
 */
/*global jht, $, jQuery, _ */

jht.google = (function($){

    "use strict";

    var stateMap = {},
        initModule, showRegions, getVideos, getVideo,
        getChannels, getChannel, getPlaylistItems,
        dataStore = {};

    getVideos = function(id){
        if(dataStore[id]){
            jht.shell.displayGrid(dataStore[id], id);
        }else {
            gapi.client.youtube.videos.list({
                part            : 'snippet,contentDetails',
                chart           : 'mostPopular',
                videoCategoryId : id,
                maxResults      : 20
            })
            .execute(function (response) {
                if(response && !response.error){
                    dataStore[id] = response;
                    jht.shell.displayGrid(response, id);
                }
            });
        }
    };

    getVideo = function(id){
        gapi.client.youtube.videos.list({
                part : 'snippet',
                id : id
            })
            .execute(function(response){
                if(response && !response.error){
                    jht.shell.loadVideo(response, id);
                }
            });
    };

    getChannels = function(){
        var request = gapi.client.youtube.guideCategories.list({
            part : 'snippet',
            regionCode : stateMap.region
        });

        request.execute(function(response) {
          // showVideoList(response);
            console.log(response);
        });
    };

    getChannel = function(id){
        gapi.client.youtube.channels.list({
            part: 'contentDetails',
            id : id

        }).execute(function(response){
            console.log(response);
        });
    };



    getPlaylistItems = function(id){
        gapi.client.youtube.playlistItems.list({
            playlistId : id,
            part:'snippet',
            maxResults: 20
        }).execute(function(response){
            jht.shell.displayGrid(response);
        });
    };

    showRegions = function(res){
        var template = _.template( $('#regionList').html() );
        $('#regions').append(template(res));
    };


    initModule = function(){

        var apiKey = config.apiKey;

        gapi.client.setApiKey( apiKey );

        gapi.client.load('youtube', 'v3', function() {
            $('.channel[data-id="0"]').trigger('click');
        });

        // set default region
        jht.region.setRegion('US');


        if(window.location.hash && _.indexOf(window.location.hash, 'access') !== -1){
            window.location = 'https://www.googleapis.com/oauth2/v1/tokeninfo?' + window.location.hash;
        }
    };

    return{
        initModule: initModule,
        stateMap : stateMap,
        getVideos : getVideos,
        getVideo: getVideo,
        getChannels : getChannels,
        getChannel : getChannel,
        getPlaylistItems : getPlaylistItems,
        showRegions : showRegions
    };

}(jQuery));






