var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    sass = require('gulp-sass'),
    minifyCss = require('gulp-minify-css');

gulp.task('lint', function() {

    gulp.src('./public/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('js', function(){

    gulp.src([
        './public/js/jht.js',
        './public/js/jht.player.js',
        './public/js/jht.region.js',
        './public/js/jht.google.js',
        './public/js/jht.shell.js'

    ])
        .pipe(uglify())
        .pipe(concat('main.min.js'))
        .pipe(gulp.dest('./public/src'));
});

gulp.task('sass', function() {
    gulp.src('./public/scss/*.scss')
        .pipe(sass())
        .pipe(minifyCss())
        .pipe(gulp.dest('./public/src'));
});

gulp.task('watch', function() {
    gulp.watch('./public/js/*.js', ['js', 'lint']);
    gulp.watch('./public/scss/*.scss', ['sass']);
});


gulp.task('default', ['js', 'lint', 'sass', 'watch']);